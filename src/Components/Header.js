import React from 'react'

const Header = () => {
  return (
    <div className="header">
      <div className="container">
        <div className="header__title">
          <h1>Your dream holiday starts here</h1>
          <span>Flying to?</span>
        </div>
      </div>

      <svg
        className="header__icon"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 84 34"
      >
        <path
          d="M.387.8h4.3l12.5 12.4 24.9 1.9-4.5-9.8h3.3l11.8 10.7 21.3 1.7s7.6.9 9.5 5.1c0 0 1.7 2.8-8.1 3.6l-20.7-1.6c-.1.3-.5.8-1.3.8l-4.1-.4h-.2l-2.9 1.1.6.2 3 .7s1.1.4 1.1 1.1v1.4s0 1.1-1.4 1.1l-4.5-.4s-2.1-.4-2.8-1.8V28l.8-.4-.4-.1-15.5 5.7h-4.6l16.1-9.6-18.1-1.4-18.3-6.6V14l4.2-.8-6-12.4z"
          fill="#FFF"
          fillRule="nonzero"
        />
      </svg>
    </div>
  )
}

export default Header
